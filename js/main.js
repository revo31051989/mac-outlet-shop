class Basket {
   constructor() {
      this.items = [];
      this.totalAmount = 0;
      this.totalPrice = 0;
      this.container = document.querySelector('.nameblok');
      this.modalBasket = document.querySelector('.modalBasket');

      const dataFromLS = this.getDataFromLocalStorage();
      if (dataFromLS) {
         Object.assign(this, dataFromLS)
      }
      this.elementBasket = document.querySelector('.__containerBasket');
      this._modalBasket();
      this.renderBasket();
   }

   getDataFromLocalStorage() {
      const data = localStorage.bas
      return JSON.parse(data || null)
   }

   setDataToLocalStorage() {
      const { modalBasket, container, elementBasket, ...data } = this;
      localStorage.bas = JSON.stringify(data)
   }

   runLogic() {
      this.setTotaPriceAndAmount()
      this.renderBasket()
      this.setDataToLocalStorage()
   }

   addToCart(data) {
      const itemInCart = this.items.find(item => item.id === data.id)
      if (itemInCart) {
         itemInCart.amount++;
         if (itemInCart.amount >= 4) itemInCart.amount = 4
      } else {
         this.items.push({
            id: data.id,
            name: data.name,
            imgUrl: data.imgUrl,
            amount: 1,
            price: data.price
         })
      }
      this.runLogic()
   }

   setTotaPriceAndAmount() {
      const newData = this.items.reduce((acc, item) => {
         return {
            totalAmount: acc.totalAmount + item.amount,
            totalPrice: acc.totalPrice + (item.price * item.amount)
         }
      }, { totalAmount: 0, totalPrice: 0 })

      Object.assign(this, newData)
   }

   _modalBasket() {
      const basketM = document.createElement('div')
      basketM.className = 'basket'
      basketM.onclick = (event) => {
         this.modalBasket.classList.add('active')
      }
      this.modalBasket.onclick = (event) => {
         if ((this.modalBasket != event.target)) return
         this.modalBasket.classList.remove('active')
      }

      basketM.innerHTML = `<i class="fas fa-shopping-cart"></i>`
      this.container.append(basketM)
   }

   removedFromBasket(data) {
      this.items = this.items.filter(item => item.id !== data.id)
      this.runLogic()
   }

   upToBasket(data) {
      let c1 = [0]
      const a = this.items.find(item => {
         if (data.amount < 4) {
            c1.push(data.amount)
            data.amount = c1[1] + 1
            return (item.id === data.id)
         }
         else {
            data.amount = 4
            return (item.id === data.id)
         }
      })
      this.runLogic()
   }

   downFromBasket(data) {
      let c1 = [0]
      const a = this.items.find(item => {
         if (data.amount > 0) {
            c1.push(data.amount)
            data.amount = c1[1] - 1
            return (item.id === data.id)
         }
         else {
            data.amount = 0
            return (item.id === data.id)
         }
      })
      this.runLogic()
   }

   renderBasket() {
      const atotal = this.elementBasket.querySelector('.atotal')
      atotal.innerHTML = `Total amount <strong>${this.totalAmount}ptc.</strong>`
      const ptotal = this.elementBasket.querySelector('.ptotal')
      ptotal.innerHTML = `Total price <strong>${this.totalPrice}$</strong>`
      const itemsConteiner = this.elementBasket.querySelector('.cartitems')
      itemsConteiner.innerHTML = ''

      const itemsAsElements = this.items.map(item => {
         const block = document.createElement('div')
         block.className = 'cartitems_item'
         block.innerHTML = `
         <img src="img/${item.imgUrl}" alt=""><strong>
               <div class="itms">
               ${item.name} 
               <p class="price">${item.price}$</p>
               </strong>               
               </div>
               <p class="down"></p>
               <p class="numbr">${item.amount}</p>
               <p class="up"></p>
               <p class="cancel"></p>
         `

         const removeFromBasket = document.createElement('button')
         removeFromBasket.className = 'cancelb'
         removeFromBasket.innerText = 'x'
         removeFromBasket.onclick = () => {
            this.removedFromBasket(item)
         }
         block.querySelector('.cancel').after(removeFromBasket)
         const updown = document.createElement('button')
         updown.className = 'upb'
         updown.innerText = '>'
         if (item.amount < 4) {
            updown.onclick = () => {
               this.upToBasket(item)
               updown.classList.remove('_none')
            }
         }
         else {
            updown.classList.add('_none')
         }
         block.querySelector('.up').after(updown)

         const downup = document.createElement('button')
         downup.className = 'downb'
         downup.innerText = '<'
         if (item.amount >= 1) {
            downup.onclick = () => {
               this.downFromBasket(item)
               downup.classList.remove('_none')
            }
         }
         else {
            downup.classList.add('_none')
         }
         block.querySelector('.down').after(downup)

         return block
      })

      const button = document.createElement('button')
      button.className = 'buy'
      button.onclick = function (event) {
         console.log(event)
      }
      button.innerText = 'buy'

      this.modalBasket.append(button)

      itemsConteiner.append(...itemsAsElements)
   }
}

class Slider {
   constructor() {
      this.container = document.querySelector('.imgblok')
      this.slick()
   }
   _createSlider(data) {
      const slider = document.createElement('div')
      slider.className = 'imgblok'
      slider.innerHTML = ''
      slider.innerHTML = `<div class="slidername"><h2>${data.name}</h2></div><img src="img/${data.imgUrl}" width ="100%" alt="">`
      const button = document.createElement('button')
      button.className = 'addToCard'
      button.onclick = function (event) {
         event.stopImmediatePropagation()
      }
      button.innerText = 'Add to cart'
      slider.append(button)
      button.classList.add("bimg")

      if (data.orderInfo.inStock === 0) {
         button.classList.add('_none')
      }
      else {
         button.classList.remove('_none')
         button.onclick = function (event) {
            event.stopImmediatePropagation()
            bas.addToCart(data)
         }
      }

      this.container.append(slider)
      return slider
   }

   slick(arr = sliders) {
      function randomInteger(min, max) {
         let rand = min + Math.random() * (max + 1 - min);
         return Math.floor(rand)
      }
      let i = randomInteger(0, arr.length - 1)
      this.container.innerHTML = ''
      const count = setInterval(() => {
         let slicker = arr.filter(item => {
            if (item.id == i) {
               this.container.innerHTML = ''
               i = randomInteger(0, arr.length - 1)
               return this._createSlider(item)
            }
         })
         clearInterval()
      }, 5000);
   }

}

class Finder {
   constructor() {
      this.container = document.querySelector('.finder')
      this.filterContainer = document.querySelector('.accFilter')
      this.mainContainer = document.querySelector('.container')
      this.sortContainer = document.querySelector('.sortmodal')
      this.createFind()
   }

   createFind() {
      const finderfind = this.container.querySelector('.finderfind')
      finderfind.innerHTML = `<img src="https://img.icons8.com/ios-glyphs/30/000000/search.png"/>`
      const findertext = this.container.querySelector('.findertext')
      findertext.innerHTML = `
      <input type="text" class="textarea">`
      const finderfiter = this.container.querySelector('.finderfiter')
      finderfiter.innerHTML = `<i class="fas fa-filter" style="color:blue"></i>`
      const finderparam = this.container.querySelector('.finderparam')
      finderparam.innerHTML = `<i class="fas fa-wrench" style="color:blue"></i>`

      finderparam.onclick = (event) => {
         this.filterContainer.classList.toggle('accFilteractiv')
         this.mainContainer.classList.toggle('containerwidth')
      }
      finderfiter.onclick = (event) => {
         this.sortContainer.classList.toggle('block')
         console.log('event')
      }
   }
}

class Card {
   constructor() {
      this.container = document.querySelector('.container')
      this.modal = document.querySelector('.modal')
      this.modalContainer = document.querySelector('.modal .__container')
      this.sortBlock = document.querySelector('.sortmodal')
      this.sortContainer = document.querySelector('.__containerSort')
      this.renderCards()
      this.renderSort()
   }

   _createCard(data) {
      const card = document.createElement('div')
      card.onclick = (event) => {
         this.renderModal(data)
      }
      card.innerHTML = ''
      card.className = 'card'
      card.innerHTML = `
		<div class="cardwhiteblock">
	<ul><li class="cardwhiteblocklr"></li>
		<li class="cardwhiteblockcntr"><div class="imghw"><img src="img/${data.imgUrl}" width="100%" alt=""></div>
		<div class="contcard">
		<h2>${data.name}</h2>
		<p><strong><i class="${data.orderInfo.inStock != 0 ? 'far fa-check-circle' : 'far fa-times-circle'}" color="${data.orderInfo.inStock === 0 ? 'red' : 'green'}"></i> ${data.orderInfo.inStock}</strong> left in stock</p>
		<p>Price:<strong> ${data.price}</strong>$</p>
		</div></li>		
		<li class="cardwhiteblocklr"></li></ul></div>
		<div class="positive_review">
		<ul>
		<li class="love"><p class="fullheart"><i class="fas fa-heart"></i></p></li>
		<li class="review"><strong>${data.orderInfo.reviews}%</strong> Positive reviewes
		<p>Above avarages</p></li>
		<li class="clc"><strong>${Math.floor(Math.random() * (1001))}<strong>
		<p>orders</p></li>
		</ul>
		</div>`

      const button = document.createElement('button')
      button.className = 'addToCard'
      button.onclick = function (event) {
         event.stopImmediatePropagation()
      }
      button.innerText = 'Add to cart'

      if (data.orderInfo.inStock === 0) {
         button.classList.add('_none')
      }
      else {
         button.classList.remove('_none')
         button.onclick = function (event) {
            event.stopImmediatePropagation()
            bas.addToCart(data)
         }
      }

      card.append(button)

      const favorites = document.createElement('button')
      favorites.className = 'fav'
      favorites.innerHTML = `<i class="far fa-heart" style="color:black"></i>`
      favorites.onclick = (e) => {
         e.stopImmediatePropagation()
         favorites.classList.toggle('fav')
         const isActive = favorites.classList.toggle('fav1')
         favorites.innerHTML = `${isActive ? `<i class="fas fa-heart" style="color:red"></i>` : `<i class="far fa-heart"></i>`} `
      }

      card.append(favorites)

      return card;
   }

   renderCards(arr = items) {
      this.container.innerHTML = ''
      const cards = arr.map(card => this._createCard(card))
      this.container.append(...cards)

   }

   renderModal(data) {
      const button = document.createElement('button')
      button.className = 'addToCard'
      button.onclick = function (event) {
         event.stopImmediatePropagation()
      }
      button.innerText = 'Add to cart'
      this.modal.append(button)
      button.classList.add("mb")

      if (data.orderInfo.inStock === 0) {
         button.classList.add('_none')
      }
      else {
         button.classList.remove('_none')
         button.onclick = function (event) {
            event.stopImmediatePropagation()
            bas.addToCart(data)
         }
      }

      this.modal.classList.add('active')
      this.modalContainer.innerHTML = `
		<ul>
	<li class="lil"><div class="modalimg"><img src="img/${data.imgUrl}" width ="200px" alt=""><div></li>
	<li class="cntr"><h1>${data.name}</h1>
	<div class="positive_reviewmodal"><p>
	<ul>
	<li class="love"><p class="fullheart"><i class="fas fa-heart"></i></p></li>
	<li class="review"> <strong>${data.orderInfo.reviews}%</strong> Positive reviewes
	<p>Above avarages</p></li>
	<li class="clc"><strong>${Math.floor(Math.random() * (1000))}</strong>
	<p>orders</p></li>
	</ul>
	</p></div>
	<p>Color: <strong>${data.color}</strong></p>
	<p>Operating System: : <strong>${data.os}</strong></p>
	<p>Chip: <strong>${data.chip.name}</strong></p>
	<p>Height: <strong>${data.size.height}</strong></p>
	<p>Width: <strong>${data.size.width}</strong></p>
	<p>Depth: <strong>${data.size.depth}</strong></p>
	<p>Weight: <strong>${data.size.weight}</strong></p>
	</li>
	<li class="lir"><h1>$ <strong>${data.price}</strong></h1>
	<p>Stock: <strong>${data.orderInfo.inStock}</strong> pcs.</p>
	 
	</li>
</ul>
		`

      this.modal.onclick = (event) => {
         if (this.modal != event.target) return
         this.modal.classList.remove('active')
      }
   }

   renderSort() {
      const order = this.sortContainer.querySelector('.orderText')
      order.innerText = 'Order'
      this.sortContainer.append(order)

      const def = this.sortContainer.querySelector('.default')
      def.innerText = 'Default'
      def.onclick = (e) => {
         console.log(e)
      }
      this.sortContainer.append(def)

      const ascend = this.sortContainer.querySelector('.ascending')
      ascend.innerHTML = `<div class="inleft"><i class="fas fa-chevron-up" ></i></div><div class="inright">Ascending</div>`
      ascend.onclick = (e) => {
         console.log(e)
      }
      this.sortContainer.append(ascend)

      const descend = this.sortContainer.querySelector('.descending')
      descend.innerHTML = `<div class="inleft"><i class="fas fa-chevron-down" ></i></div><div class="inright">Descending</div>`
      descend.onclick = (e) => {
         console.log(e)
      }
      this.sortContainer.append(descend)
   }

}

class FilterService {
   constructor() {
      this.price = this.initialPrice
      this.color = []
      this.os = []
      this.ram = []
      this.display = []
      this.search = ''
   }

   get initialPrice() {
      const sortedArr = [...items].sort((a, b) => a.price - b.price)
      return [sortedArr[0].price, sortedArr[sortedArr.length - 1].price]
   }

   get initialColor() {
      return items.reduce((acc, item) => {
         item.color.forEach(colorInArr => {
            if (acc.includes(colorInArr)) return
            acc.push(colorInArr)
         })
         return acc
      }, [])
   }

   get initialMemory() {
      return items.reduce((acc, item) => {
         if (item.ram != null && !acc.includes(item.ram)) {
            acc.push(item.ram)
         }
         return acc
      }, [])
   }

   get initialDisplay() {
      return items.reduce((acc, item) => {
         if (item.display < 5 || item.display == null) {
            item.display = '0-5'
         }
         if (item.display >= 5 && item.display < 7) {
            item.display = '5-7'
         }
         if (item.display >= 7 && item.display < 12) {
            item.display = '7-12'
         }
         if (item.display >= 12 && item.display < 16) {
            item.display = '12-16'
         }
         if (item.display >= 16) {
            item.display = '16+'
         }
         if (!acc.includes(item.display)) {
            acc.push(item.display)
         }
         return acc
      }, [])
   }

   get initialOs() {
      return items.reduce((acc, item) => {
         if (item.os != null && !acc.includes(item.os)) {
            acc.push(item.os)
         }
         return acc
      }, [])
   }

   changeSearch(value) {
      const textarea = document.querySelector('.findertext')

      textarea.oninput = e => {
         const res = items.filter(item => {

         })
      }


      this.search = value
      this.filterAndRender()
   }

   changePrice(value, type) {
      if (type === 'from') {
         this.price[0] = value
      } else {
         this.price[1] = value
      }
      this.filterAndRender()
   }

   changeColor(color) {
      if (this.color.includes(color)) {
         this.color = this.color.filter(c => c !== color)
      } else {
         this.color.push(color)
      }
      this.filterAndRender()
   }

   changeOs(os) {
      if (this.os.includes(os)) {
         this.os = this.os.filter(c => c !== os)
      } else {
         this.os.push(os)
      }
      this.filterAndRender()
   }

   changeMemory(ram) {
      if (this.ram.includes(ram)) {
         this.ram = this.ram.filter(c => c !== ram)
      } else {
         this.ram.push(ram)
      }
      this.filterAndRender()
   }

   changeDisplay(display) {
      if (this.display.includes(display)) {
         this.display = this.display.filter(c => c !== display)
      } else {
         this.display.push(display)
      }
      this.filterAndRender()
   }

   filterArrByOptions() {
      return items.filter(item => {
         if (!(item.name.toLocaleLowerCase().includes(this.search.toLocaleLowerCase()))) return

         if (!(item.price >= this.price[0] && item.price <= this.price[1])) return

         let isColor = !this.color.length || this.color.some(c => item.color.includes(c))
         if (!isColor) return

         let isOs = !this.os.length || this.os.includes(item.os)
         if (!isOs) return

         let isMemory = !this.ram.length || this.ram.includes(item.ram)
         if (!isMemory) return

         let isDisplay = !this.display.length || this.display.includes(item.display)
         if (!isDisplay) return

         return true
      })
   }

   filterAndRender() {
      const filteredArr = this.filterArrByOptions()
      cards.renderCards(filteredArr)
   }
}

class FilterRender {
   constructor() {
      this.filterService = new FilterService()
      this.options = [
         {
            title: 'Price',
            options: this.filterService.initialPrice,
            setter: this.filterService.changePrice.bind(this.filterService),
         },
         {
            title: 'Color',
            options: this.filterService.initialColor,
            setter: this.filterService.changeColor.bind(this.filterService),
         },
         {
            title: 'Os',
            options: this.filterService.initialOs,
            setter: this.filterService.changeOs.bind(this.filterService),
         },
         {
            title: 'Memory',
            options: this.filterService.initialMemory,
            setter: this.filterService.changeMemory.bind(this.filterService),
         },
         {
            title: 'Display',
            options: this.filterService.initialDisplay,
            setter: this.filterService.changeDisplay.bind(this.filterService),
         }
      ]
      this.renderFilter()
   }

   renderFilter() {
      const filterContainer = document.querySelector('.accFilter')
      this.options.forEach(data => {
         const block = document.createElement('div')
         block.className = 'block'


         const header = document.createElement('div')
         header.className = '__header'
         header.innerHTML = `
			${data.title}
			<i class="fas fa-chevron-right"></i>
			`
         header.onclick = () => {
            header.classList.toggle('--header_active')
            const isActive = container.classList.toggle('--active')
            header.innerHTML = `${data.title}
				<i class="fas fa-chevron-${isActive ? 'down' : 'right'}"></i>
				`
         }

         const container = document.createElement('div')
         container.className = '___container'

         if (data.title === 'Price') {
            const labelFrom = document.createElement('label')
            labelFrom.innerText = 'From'

            const inputFrom = document.createElement('input')
            inputFrom.value = data.options[0]
            inputFrom.oninput = e => {
               data.setter(e.target.value, 'from')
            }
            labelFrom.appendChild(inputFrom)

            const labelTo = document.createElement('label')
            labelTo.innerText = 'To'

            const inputTo = document.createElement('input')
            inputTo.value = data.options[1]
            inputTo.oninput = e => {
               data.setter(e.target.value, 'to')
            }
            labelTo.appendChild(inputTo)
            container.append(labelFrom, labelTo)


         } else {
            data.options.forEach(options => {

               const label = document.createElement('label')
               label.innerHTML = options

               const input = document.createElement('input')
               input.type = 'checkbox'
               input.onchange = e => {
                  data.setter(options)
               }

               label.appendChild(input)
               container.appendChild(label)
            })
         }

         block.append(header, container)
         filterContainer.appendChild(block)
      })
   }
}

const fr = new FilterRender()
const bas = new Basket()
const slider = new Slider()
const find = new Finder()
const cards = new Card()