const sliders = [
   {
      id: 1,
      imgUrl: 'banners/air_pods_max_banner.jpg',
      name: 'Air Pods Max',
      price: 599,
      orderInfo: {
         inStock: 0,
         reviews: 5,
      }
   },
   {
      id: 2,
      imgUrl: 'banners/iphone_12_banner.jpg',
      name: 'Iphone 12',
      price: 799,
      orderInfo: {
         inStock: 73,
         reviews: 12,
      }
   },
   {
      id: 3,
      imgUrl: 'banners/airpods_pro_banner.png',
      name: 'Air Pods Pro',
      price: 299,
      orderInfo: {
         inStock: 29,
         reviews: 94,
      }
   },
   {
      id: 4,
      imgUrl: 'banners/apple_tv_banner.png',
      name: 'Apple Tv',
      price: 199,
      orderInfo: {
         inStock: 1,
         reviews: 100,
      }
   },
   {
      id: 5,
      imgUrl: 'banners/ipad_air_banner.jpg',
      name: 'Ipad Air',
      price: 549,
      orderInfo: {
         inStock: 97,
         reviews: 97,
      }
   },
   {
      id: 6,
      imgUrl: 'banners/mac_book_banner.jpg',
      name: 'Mac Book',
      price: 1249,
      orderInfo: {
         inStock: 0,
         reviews: 84,
      }
   },
   {
      id: 0,
      imgUrl: 'banners/watch_banner.jpg',
      name: 'Watch',
      price: 399,
      orderInfo: {
         inStock: 90, 
         reviews: 458, 
      }
   },
];